Appli1
------------
**Appli1** est une petite appli permettant d'afficher des personnages et leurs armes.<br>
Cet exercice traite des :
* Bases du Framework PHP symfony.
* Bases de routage et de structuration d'une application Symfony. 
* Notions fondamentales de PHP (tableaux associatif, POO, et MVC).

![img](https://i.imgur.com/gEdzVp7.png)

<a href="https://appli1.tomspace.net" target="_blank">Voir l'appli en ligne</a>

Lancer l'appli en local :
```bash
$ php -S 0.0.0.0:8888 -t public/
```
