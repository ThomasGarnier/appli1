<?php

namespace App\Controller;

use App\Entity\Personnage;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PersonnageController extends AbstractController
{
    /**
     * @Route("/", name="accueil")
     */
    public function index()
    {
        return $this->render('personnage/index.html.twig');
    }

    /**
     * @Route("/persos", name="personnages")
     */
    public function persos()
    {
        Personnage::creerPersonnages();

        return $this->render('personnage/persos.html.twig', [
            'players' => Personnage::$personnage,
        ]);
    }

    /**
     * @Route("/persos/{nom}", name="afficher_personnage")
     *
     * @param mixed $nom
     */
    public function afficherPerso($nom)
    {
        Personnage::creerPersonnages();
        $perso = Personnage::getPersonnageParNom($nom);

        return $this->render('personnage/perso.html.twig', [
            'perso' => $perso,
        ]);
    }
}
